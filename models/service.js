var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let ServiceSchema = new Schema({
    title: String,
    resum: String,
    content: String,
    about: String,
    prise: String,
    pourcentage: String,
}, {
    timestamps: true
});
exports.ServiceModel = Mongoose.model("Service", ServiceSchema);