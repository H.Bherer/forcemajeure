var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let PropoSchema = new Schema({
    subTitle1: String,
    content: String,
    subTitle2: String,
    subTitle3: String,

}, {
    timestamps: true
});
exports.PropoModel = Mongoose.model("Propo", PropoSchema);